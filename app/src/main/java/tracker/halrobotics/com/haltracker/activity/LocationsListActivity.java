package tracker.halrobotics.com.haltracker.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.R;

/**
 * Created by mac on 14/08/18.
 */

public class LocationsListActivity extends AppBaseActivity implements CustomListAdapterInterface {
    private ListView listView;
    private CustomListAdapter<JSONObject> adapter;
    private List<JSONObject> locationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_location_list);
        listView = findViewById(R.id.listView);
        JSONArray array = Preferences.getLocationArray();
        try {
            for (int i = 0; i < array.length(); i++) {
                locationList.add(array.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter = new CustomListAdapter<>(this, android.R.layout.simple_list_item_2, locationList, this);
        listView.setAdapter(adapter);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        JSONObject json = locationList.get(position);
        String speed = json.optString("speed");
        String calculatedSpeed = json.optString("calculatedSpeed");
        holder.tv1.setText("Speed: " + speed + " \nCalculated Speed: " + calculatedSpeed);
        long time = json.optLong("deviceTime");
        holder.tv2.setText("Time: " + new Date(time));
        return convertView;
    }

    private static class ViewHolder {
        TextView tv1, tv2;

        public ViewHolder(View v) {
            tv1 = v.findViewById(android.R.id.text1);
            tv2 = v.findViewById(android.R.id.text2);
        }
    }
}
