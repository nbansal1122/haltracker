package tracker.halrobotics.com.haltracker.location.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("BootCompletedReceiver", intent.getAction());
        LocationTrackerUtil.scheduleJob(context);
        LocationTrackerUtil.startTracker(context);
    }
}
