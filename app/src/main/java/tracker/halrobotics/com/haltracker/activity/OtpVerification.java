package tracker.halrobotics.com.haltracker.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.R;
import tracker.halrobotics.com.haltracker.model.BaseResponse;
import tracker.halrobotics.com.haltracker.model.HalConfigResponse;
import tracker.halrobotics.com.haltracker.util.ApiGenerator;

public class OtpVerification extends AppBaseActivity {
    private String otp;
    private String mobile;

    @Override
    protected void loadBundle(Bundle bundle) {
        this.mobile = bundle.getString(AppConstants.BUNDLE_KEYS.MOBILE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        setOnClickListener(R.id.btn_submit);
    }



    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_submit:
                if(isValid()){
                    verifyOtp();
                }
                break;
        }
    }

    private void verifyOtp() {
        HttpParamObject req = ApiGenerator.verifyOtp(mobile, otp);
        this.executeTask(AppConstants.TASK_CODES.VERIFY_OTP, req);
    }

    private void updateDeviceInfo() {
        HttpParamObject req = ApiGenerator.postDeviceInfo(this, mobile);
        this.executeTask(AppConstants.TASK_CODES.DEVICE_INFO, req);
    }

    private void getConfig() {
        HttpParamObject req = ApiGenerator.getConfigData(mobile);
        this.executeTask(AppConstants.TASK_CODES.GET_CONFIG, req);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.VERIFY_OTP: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse != null && baseResponse.isStatus()) {
                    updateDeviceInfo();
                } else {
                    showToast("Invalid OTP! Please try again");
                }
            }
            break;
            case AppConstants.TASK_CODES.DEVICE_INFO: {
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse != null && baseResponse.isStatus()) {
                    getConfig();
                } else {
                    showToast("Error while logging in, Please try again to continue");
                    startNextActivity(MobileActivity.class);
                    finish();
                }
            }
            break;
            case AppConstants.TASK_CODES.GET_CONFIG: {
                HalConfigResponse configResponse = (HalConfigResponse) response;
                if (configResponse != null && configResponse.getFrequency() > 0.0) {
                    saveDataAndProceed(configResponse.getFrequency());
                } else {
                    showToast("Error while getting the information from server, Please try again to continue");
                    startNextActivity(MobileActivity.class);
                    finish();
                }
            }
            break;
        }
    }

    private void saveDataAndProceed(double freq) {
        long frequency = (long) (freq * 1000 * 60);
        Preferences.setUserLoggedIn(true);
        Preferences.saveData(Preferences.KEY_MOBILE, mobile);
        Preferences.saveData(Preferences.KEY_FREQUENCY, frequency);
        startNextActivity(HomeActivity.class);
        finish();
    }


    private boolean isValid() {
        otp = getEditText(R.id.et_otp);
        if (TextUtils.isEmpty(otp) || otp.length() != 4) {
            showToast("Please enter a valid 4 digit OTP");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startNextActivity(MobileActivity.class);
        finish();
    }
}
