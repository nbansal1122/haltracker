package simplifii.framework.asyncmanager;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.Util;

/**
 * Created by mac on 25/10/17.
 */

public class MockAssetService extends MockJsonService {
    public MockAssetService(Context context) {
        super(context);
    }

    @Override
    public Object getData(Object... params) throws JSONException, SQLException, NullPointerException, RestException, ClassCastException, IOException {
        if (params != null && params.length > 0) {
            HttpParamObject param = (HttpParamObject) params[0];
            String json = Util.getStringFromAssets("json/"+param.getUrl(), context);
            if (!TextUtils.isEmpty(json)) {
                return parseJson(json, param);
            }else{
                throw new RestException(404, "Data not found");
            }
        }
        return null;
    }
}
