package tracker.halrobotics.com.haltracker;

import android.*;
import android.Manifest;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.activity.AppBaseActivity;
import tracker.halrobotics.com.haltracker.activity.HomeActivity;
import tracker.halrobotics.com.haltracker.activity.MobileActivity;
import tracker.halrobotics.com.haltracker.location.service.LocationTrackerUtil;
import tracker.halrobotics.com.haltracker.util.TMUtil;

public class SplashActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        askPermissions();
    }

    private void askPermissions() {
        TedPermission.with(this).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION).setPermissionListener(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
//                LocationTrackerUtil.scheduleJob(SplashActivity.this);
                moveToNext();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                finish();
            }
        }).check();
    }

    private void moveToNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startNext();
            }
        }, 1000);
    }

    private void startNext() {
        if (Preferences.isUserLoggerIn()) {
            startNextActivity(HomeActivity.class);
        } else {
            startNextActivity(MobileActivity.class);
        }
        finish();
    }
}
