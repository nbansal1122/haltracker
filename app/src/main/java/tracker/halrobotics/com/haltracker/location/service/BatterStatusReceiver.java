package tracker.halrobotics.com.haltracker.location.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;

/**
 * Created by mac on 31/07/18.
 */

public class BatterStatusReceiver extends BroadcastReceiver {
    private static final String TAG = "BSReceiver";
    @Override
    public void onReceive(Context context, Intent batteryStatus) {
        Log.d(TAG, "Battery Status Receiver:");
        if(batteryStatus != null && batteryStatus.getExtras() != null){
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPct = level / (float)scale;
            Log.d(TAG, "Battery Pct:"+batteryPct);
        }

    }
}
