package tracker.halrobotics.com.haltracker;

import android.app.Application;

import simplifii.framework.utility.Preferences;

/**
 * Created by mac on 28/07/18.
 */

public class AppController extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Preferences.initSharedPreferences(this);
    }
}
