package tracker.halrobotics.com.haltracker.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.R;
import tracker.halrobotics.com.haltracker.location.service.LocationTrackerUtil;

public class HomeActivity extends AppBaseActivity {

    private static final int REQ_CODE_GET_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setOnClickListener(R.id.btn_submit);
        boolean isTrackingStarted = Preferences.getData(Preferences.KEY_IS_TRACKING_STARTED, false);
        long freq = Preferences.getData(Preferences.KEY_FREQUENCY, 0l);
        Log.d(TAG, "Freq is: " + freq);
        if (isTrackingStarted) {
            showToast("Tracking is ON");
//            checkGPSenable();
        }
        setOnClickListener(R.id.btn_view_locations);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit:
                checkGPSenable();
                break;
            case R.id.btn_view_locations:
                startNextActivity(LocationsListActivity.class);
                break;
        }
    }

    private void checkGPSenable() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocationPermission();
        } else {
            buildAlertMessageNoGps();
        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_error)
                .setCancelable(false)
                .setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        setLocation();
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQ_CODE_GET_LOCATION);
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_GET_LOCATION) {
            checkGPSenable();
        }
    }

    public void getLocationPermission() {
        TedPermission.with(this).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        startTracking();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).check();
    }

    private void startTracking() {
        LocationTrackerUtil.startTracker(this);
        Preferences.saveData(Preferences.KEY_IS_TRACKING_STARTED, true);
        showToast(getString(R.string.msg_tracking_started));
        finish();
    }
}
