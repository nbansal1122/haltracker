package tracker.halrobotics.com.haltracker.location.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.RequiresApi;
import android.util.Log;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class SyncJobService extends JobService {
    private static final String TAG = "SyncService";
    private static final int NOTIFICATION_ID = 1003;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "onStartJob");
        PersistableBundle bundle = params.getExtras();
        double interval = bundle.getDouble("interval", 0.0);
        // Toast.makeText(SyncJobService.this, params.getJobId() + " " + interval, Toast.LENGTH_LONG).show();
        Log.d(getPackageName(), "Tracker Service Start from JOB ");
        LocationTrackerUtil.startTracker(getApplicationContext());
        LocationTrackerUtil.scheduleJob(this);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(getPackageName(), "onStopJob");
        return true;
    }

}