package simplifii.framework.utility;

import android.app.Activity;

import java.util.LinkedHashMap;

public interface AppConstants {

    public static final String DEF_REGULAR_FONT = "Roboto-Regular.ttf";
    public static final String SF_SPECIAL_ACTION_LOGOUT = "LOGOUT";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer, String> storeCategory = new LinkedHashMap<Integer, String>();


    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }

    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String BASE_URL = "http://40.122.31.138:3000/api/";
        String OTP_REQUEST = BASE_URL + "otp-request";
        String OTP_VERIFICATION = BASE_URL + "otp-verification";
        String DEVICE_INFO = BASE_URL + "device-info";

        String GET_CONFIG = BASE_URL + "config/";
        String POST_DATA = BASE_URL + "data/";
    }

    interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";
        String KEY_USER_DATA = "userData";
        String FCM_TOKEN = "fcm_token";
        String USER_SESSION = "userSession";
        java.lang.String USER_TOKEN = "userToken";
        String TOCKEN_FCM = "fcmToken";
        java.lang.String KEY_LOCATION_SWITCH = "sfLocationSwitch";
        String LOGIN_API = "loginApi";
        String LOGIN_API_METHOD = "loginApiMethod";
        java.lang.String ALARM_TIME = "alarmTime";
        java.lang.String KEY_LOCATIONS_JSON = "keyLocationJson";
        java.lang.String TRACKING_START_TIME = "trackingStartTime";
        java.lang.String TRACKING_END_TIME = "trackingEndTime";
        String LAST_ATTENDANCE_RECORDED_AT = "last_attendance_recorded_at";
        String LAST_ATTENDANCE_SKIP_AT = "last_attendance_skip_at";
    }

    public static interface BUNDLE_KEYS {

        String MOBILE = "mobile";
    }

    public interface TASK_CODES {

        int VALIDATE_OTP = 1;
        int UPLOAD_FILE = 2;
        int FORM_DATA_SERVICE = 22;
        int HOME_FOOTER_TABS = 27;
        int SF_MASTER_JSON = 28;
        int CARD_MAPPING_JSON = 29;
        int CARD_DETAIL_JSON = 32;
        int FILTER_JSON = 35;
        int LIST_VIEW_JSON = 36;
        int CARD_ACTIONS_JSON = 46;
        int FEED_VIEW_JSON = 47;

        int SEND_OTP = 3;
        int VERIFY_OTP = 4;
        int DEVICE_INFO = 5;
        int GET_CONFIG = 6;
        int POST_LOCATION = 7;
    }


    public static interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
    }


    interface JSON {
        String CARD_MAPPING_JSON = "cards_mapping.json";
        String FILTER_JSON = "filters.json";
        String FEED_JSON = "feeds.json";
        String LIST_VIEW_JSON = "listview.json";
        String FEED_VIEW_JSON = "feedview.json";
        String CONSTANTS_JSON = "constants.json";
    }

}
