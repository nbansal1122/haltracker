package tracker.halrobotics.com.haltracker.location.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.OKHttpService;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.R;
import tracker.halrobotics.com.haltracker.SplashActivity;
import tracker.halrobotics.com.haltracker.model.BaseResponse;
import tracker.halrobotics.com.haltracker.model.HalConfigResponse;
import tracker.halrobotics.com.haltracker.util.ApiGenerator;

/**
 * Created by devdeeds.com on 27-09-2017.
 */

public class LocationMonitoringService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = LocationMonitoringService.class.getSimpleName();
    private static final String NOTIFICATION_CHANNEL_ID = "111";
    private static final int NOTIFICATION_ID = 1001;
    GoogleApiClient mLocationClient;
    //    LocationRequest mLocationRequest;
    //    String email;
    final Handler handler = new Handler();
    int count = 0;
    int location = 0;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "service OnCreate");
        try {
            Intent intent = new Intent();
            String manufacturer = Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            }
            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                if (isMyServiceRunning(LocationMonitoringService.class)) {
                    startActivity(intent);
                    Toast.makeText(this, "Please enable running in background (Auto-Start) for application", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
        }
        mLocationClient = new GoogleApiClient.Builder(LocationMonitoringService.this)
                .addConnectionCallbacks(LocationMonitoringService.this)
                .addOnConnectionFailedListener(LocationMonitoringService.this)
                .addApi(LocationServices.API)
                .build();
        //      mLocationRequest = new LocationRequest();
        startInForeground();
    }

    long fastinterval = 1000 * 60;
    float dist = 1;

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.e(TAG, "service OnStartCommand");
        if (mLocationClient == null) {
            mLocationClient = new GoogleApiClient.Builder(LocationMonitoringService.this)
                    .addConnectionCallbacks(LocationMonitoringService.this)
                    .addOnConnectionFailedListener(LocationMonitoringService.this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mLocationClient != null && !mLocationClient.isConnected()) {
            mLocationClient.connect();
        }
        count++;
        Log.d(TAG, "on Start Command count -  " + startId + "    location - " + location);

        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /*
     * LOCATION CALLBACKS
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.

            return;
        }
        long freq = Preferences.getData(Preferences.KEY_FREQUENCY, 0l);
        if (freq > 0l) {
            fastinterval = freq;
        }
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setSmallestDisplacement(dist);
        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes
        mLocationRequest.setPriority(priority);
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        Log.d(TAG, "Connected to Google API");
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection Suspended");
    }

    //to get the location change
    @Override
    public void onLocationChanged(final Location location) {
        Log.d(TAG, "Location changed");
        PostLocation postLocation = new PostLocation();
        postLocation.markLocation(this, location, null);
        postLocationToServer();
    }

    private void postLocationToServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONArray array = Preferences.getLocationArray();
                if (array.length() > 0) {
                    try {
                        HttpParamObject req = ApiGenerator.postLocationData(array);
                        Log.d(TAG, "JSON:" + req.getJson());
                        OKHttpService service = new OKHttpService();
                        HalConfigResponse response = (HalConfigResponse) service.getData(req);
                        if (response.getCode() == 405) {
                            Preferences.deleteAllData();
                            LocationTrackerUtil.stopTracker(LocationMonitoringService.this);
                        } else if (response.getCode() == 200) {
                            Preferences.clearPrevLocation();
                            if (response.getFrequency() >= 0.0) {
                                long freq = (long) (response.getFrequency() * 60 * 1000);
                                Preferences.saveData(Preferences.KEY_FREQUENCY, freq);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Failed to connect to Google API");
    }

    private void startInForeground() {
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("App is running")
                .setContentText("Location Tracking")
                .setTicker("HAL Robotics Tracking")
                .setContentIntent(pendingIntent);
        Notification notification = builder.build();
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "HAL ROBOTICS", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("App is running");
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
        startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Intent intent = new Intent();
//        intent.setAction("yogesh.pc.trackerservice.receiver");
//        sendBroadcast(intent);
        Log.d(TAG, "onDestroy");
        stopForeground(true);
        LocationTrackerUtil.startTracker(this);
        LocationTrackerUtil.scheduleJob(this);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
//        Intent intent = new Intent();
//        intent.setAction("yogesh.pc.trackerservice.receiver");
//        sendBroadcast(intent);
        Log.d(TAG, "On Task Removed");
        LocationTrackerUtil.startTracker(this);
        LocationTrackerUtil.scheduleJob(this);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}