package tracker.halrobotics.com.haltracker.model;

/**
 * Created by mac on 29/07/18.
 */

public class BaseResponse {
    private boolean status;
    private int code;
    private String msg;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
