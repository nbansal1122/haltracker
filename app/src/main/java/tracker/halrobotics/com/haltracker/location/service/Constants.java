package tracker.halrobotics.com.haltracker.location.service;


/**
 * Created by USER on 12/19/2017.
 */

public class Constants {

    public static String prefrenceName = "LocationSimplifiPref";
    public static String MOBILE_TYPE_ANDROID = "1";
    public static long FASTEST_LOCATION_INTERVAL = 1;
    public static float SMALLEST_DISTANCE = 10;


    interface FirebaseDatabaseFeilds {
        String DATE_FORMAT = "E, dd MMM yyyy hh:mm:ss a zzzz";
        String UNSYNCED_USER_LOCATIONS = "unsynced_user_locations";
        String LAT = "lat";
        String LNG = "lng";
        String TIME = "time";
        String ADDRESS = "address";
        String USER_ID = "user_id";
        String LOCALITY = "locality";
        String STATUS = "status";
        String ONLINE = "online";
        String OFFLINE = "offline";
        String CORRECT_TIME = "correct_time";
        String TIMESTAMP_MISMATCH = "timestamp_mismatch";
    }

}
