package tracker.halrobotics.com.haltracker.util;

import android.util.Log;

import com.google.android.gms.common.util.NumberUtils;

import java.text.DecimalFormat;

/**
 * Created by mac on 09/08/18.
 */

public class DistanceUtil {
    // Helper function to convert degrees to radians
    public static double DegToRad(double deg) {
        return (deg * Math.PI / 180);
    }

    // Helper function to convert radians to degrees
    public static double RadToDeg(double rad) {
        return (rad * 180 / Math.PI);
    }

    // Calculate the (initial) bearing between two points, in degrees
//    double CalculateBearing(Location startPoint, Location endPoint) {
//        double lat1 = DegToRad(startPoint.latitude);
//        double lat2 = DegToRad(endPoint.latitude);
//        double deltaLon = DegToRad(endPoint.longitude - startPoint.longitude);
//
//        double y = Math.sin(deltaLon) * Math.cos(lat2);
//        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLon);
//        double bearing = Math.atan2(y, x);
//
//        // since atan2 returns a value between -180 and +180, we need to convert it to 0 - 360 degrees
//        return (RadToDeg(bearing) + 360) % 360;
//    }

//    // Calculate the destination point from given point having travelled the given distance (in km), on the given initial bearing (bearing may vary before destination is reached)
//    Location CalculateDestinationLocation(Location point, double bearing, double distance) {
//
//        distance = distance / radius; // convert to angular distance in radians
//        bearing = DegToRad(bearing); // convert bearing in degrees to radians
//
//        double lat1 = DegToRad(point.latitude);
//        double lon1 = DegToRad(point.logintude);
//
//        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance) + Math.cos(lat1) * Math.sin(distance) * Math.cos(bearing));
//        double lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(distance) * Math.cos(lat1), Math.cos(distance) - Math.sin(lat1) * Math.sin(lat2));
//        lon2 = (lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalize to -180 - + 180 degrees
//
//        return new Location(RadToDeg(lat2), RadToDeg(lon2));
//    }

    // Calculate the distance between two points in km
    public static double CalculateDistanceBetweenLocations(double startLat, double endLat, double startLng, double endLng) {

        double lat1 = DegToRad(startLat);
        double lon1 = DegToRad(startLng);

        double lat2 = DegToRad(endLat);
        double lon2 = DegToRad(endLng);


        double deltaLat = lat2 - lat1;
        double deltaLon = lon2 - lon1;

        double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - 1));

        return (6371 * c);
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(DegToRad(lat1)) * Math.sin(DegToRad(lat2)) + Math.cos(DegToRad(lat1)) * Math.cos(DegToRad(lat2)) * Math.cos(DegToRad(theta));
        dist = Math.acos(dist);
        dist = RadToDeg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        DecimalFormat df = new DecimalFormat("#.###");
        dist = Double.parseDouble(df.format(dist));
        Log.d("FormattedDist", dist + "");
        return (dist);
    }
}
