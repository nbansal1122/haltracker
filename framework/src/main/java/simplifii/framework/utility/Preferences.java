package simplifii.framework.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;


/**
 * Created by nitin on 24/08/15.
 */
public class Preferences {

    private static final String PREF_NAME = "HALROBOTICS";
    public static final String LOGIN_KEY = "isUserLoggedIn";
    public static final String BASE_URL = "BaseApiUrl";
    public static final String KEY_LOCATIONS_JSON = "LocationJSONArray";
    public static final java.lang.String KEY_CONFIG_JSON = "configJson";
    public static final String KEY_MOBILE = "mobileNumber";
    public static final String KEY_IS_TRACKING_STARTED = "isTrackingStarted";
    public static final String KEY_FREQUENCY = "frequency";
    public static final String KEY_LAST_LOCATION_JSON = "lastLocationJson";

    private static SharedPreferences xebiaSharedPrefs;
    private static SharedPreferences.Editor editor;
    private static Preferences sharedPreferenceUtil;

    public static void initSharedPreferences(Context context) {
        sharedPreferenceUtil = new Preferences();
        sharedPreferenceUtil.xebiaSharedPrefs = context.getSharedPreferences(
                PREF_NAME, Activity.MODE_PRIVATE);
        sharedPreferenceUtil.editor = sharedPreferenceUtil.xebiaSharedPrefs
                .edit();
    }

    public static Preferences getInstance() {

        return sharedPreferenceUtil;
    }

    private Preferences() {
        // TODO Auto-generated constructor stub
    }

    public static synchronized boolean saveData(String key, String value) {
        if (value != null) {
            editor.putString(key, value);
            return editor.commit();
        }
        return false;
    }

    public static synchronized boolean saveData(String key, boolean value) {
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, long value) {
        editor.putLong(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, float value) {
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, int value) {
        editor.putInt(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, Set<String> value) {
        editor.putStringSet(key, value);
        return editor.commit();
    }

    public static boolean isUserLoggerIn() {
        boolean isLoggedIn = getData(LOGIN_KEY, false);
        Log.d("Is Logged In", isLoggedIn+"");
        return isLoggedIn;
    }

    public static void setUserLoggedIn(boolean isLoggedIn) {
        saveData(LOGIN_KEY, isLoggedIn);
    }

	/*
     * public synchronized boolean saveData(String key, Set<String> value) {
	 * //editor.putStringSet(key, value); return editor.commit(); }
	 */

    public static synchronized boolean removeData(String key) {
        editor.remove(key);
        return editor.commit();
    }

    public static synchronized Boolean getData(String key, boolean defaultValue) {
        return xebiaSharedPrefs.getBoolean(key, defaultValue);
    }

    public static synchronized String getData(String key, String defaultValue) {
        return xebiaSharedPrefs.getString(key, defaultValue);
    }

    public static synchronized float getData(String key, float defaultValue) {
        return xebiaSharedPrefs.getFloat(key, defaultValue);
    }

    public static synchronized int getData(String key, int defaultValue) {
        return xebiaSharedPrefs.getInt(key, defaultValue);
    }

    public static synchronized long getData(String key, long defaultValue) {
        return xebiaSharedPrefs.getLong(key, defaultValue);
    }

    public static synchronized Set<String> getData(String key) {
        return xebiaSharedPrefs.getStringSet(key, null);
    }

    /*
     * public synchronized Set<String> getData(String key, Set<String> defValue)
     * {
     *
     * // return naukriSharedPreferences.getStringSet(key, defValue); return
     * null; }
     */
    public static synchronized void deleteAllData() {
        editor.clear();
        editor.commit();
    }


    public static synchronized void saveLocation(JSONObject locationJson) {
        Preferences.saveData(KEY_LAST_LOCATION_JSON, locationJson.toString());
        JSONArray array = getLocationArray();
        array.put(locationJson);
        saveLocationArrayJson(array);
        Log.d("JsonCount", array.length()+"");
    }

    public static synchronized JSONObject getLastSavedLocation() {
        String lastLocationJson = Preferences.getData(KEY_LAST_LOCATION_JSON, "");
        if(!TextUtils.isEmpty(lastLocationJson)){
            try {
                return new JSONObject(lastLocationJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static synchronized void saveLocationArrayJson(JSONArray array) {
        saveLocationString(array.toString());
    }

    public static synchronized void saveLocationString(String json) {
        Preferences.saveData(KEY_LOCATIONS_JSON, json);
    }

    public static synchronized JSONArray getLocationArray() {
        String jsonArray = Preferences.getData(KEY_LOCATIONS_JSON, "");
        if (!TextUtils.isEmpty(jsonArray)) {
            try {
                JSONArray array = new JSONArray(jsonArray);
                return array;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return new JSONArray();
    }


    public static synchronized void clearPrevLocation() {
        Preferences.saveData(KEY_LOCATIONS_JSON, "");
    }
}
