package simplifii.framework.asyncmanager;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLException;

import okhttp3.OkHttpClient;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.Logger;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 03/10/17.
 */

public class MockJsonService extends OKHttpService {

    private static final String TAG = "MockJsonService";

    public MockJsonService(Context context) {
        this.context = context;
    }

    @Override
    public Object getData(Object... params) throws JSONException, SQLException, NullPointerException, RestException, ClassCastException, IOException {
        if (params != null && params.length > 0) {
            HttpParamObject param = (HttpParamObject) params[0];
//            String json = Util.getStringFromAssets(param.getUrl(), context);
            String json = getJsonFromConfig(param.getUrl());
            if (!TextUtils.isEmpty(json)) {
                return parseJson(json, param);
            }else{
                Logger.error(TAG, "Raising exception for "+param.getUrl());
                throw new RestException(500, "");
            }
        }
        return null;
    }

    private String getJsonFromConfig(String key) throws JSONException {
        String configJsons = Preferences.getData(Preferences.KEY_CONFIG_JSON, null);
        if (!TextUtils.isEmpty(configJsons)) {
            JSONObject root = new JSONObject(configJsons);
            JSONObject obj = root.optJSONObject(key);
            if (obj != null) {
                Log.d(TAG, key+" : "+obj);
                return obj.toString();
            }
        }
        return null;
    }
}
