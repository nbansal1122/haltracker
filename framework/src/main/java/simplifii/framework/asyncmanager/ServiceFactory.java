package simplifii.framework.asyncmanager;

import android.content.Context;

import simplifii.framework.utility.AppConstants;


public class ServiceFactory {

    public static Service getInstance(Context context, int taskCode) {
        Service service = null;
        switch (taskCode) {
            case AppConstants.TASK_CODES.UPLOAD_FILE:
//                service = new FileUploadService();
//                break;
            case AppConstants.TASK_CODES.FORM_DATA_SERVICE:
                service = new FormDataService();
                break;
            case AppConstants.TASK_CODES.HOME_FOOTER_TABS:
            case AppConstants.TASK_CODES.SF_MASTER_JSON:
            case AppConstants.TASK_CODES.CARD_MAPPING_JSON:
            case AppConstants.TASK_CODES.CARD_DETAIL_JSON:
            case AppConstants.TASK_CODES.LIST_VIEW_JSON:
            case AppConstants.TASK_CODES.FILTER_JSON:
            case AppConstants.TASK_CODES.CARD_ACTIONS_JSON:
            case AppConstants.TASK_CODES.FEED_VIEW_JSON:
                service = new MockJsonService(context);
                break;
//            case AppConstants.TASK_CODES.CARD_MAPPING_JSON:
//            case AppConstants.TASK_CODES.CARD_DETAIL_JSON:
//                service = new MockAssetService(context);
//                break;
            default:
                service = new OKHttpService();
                break;
        }
        return service;
    }

}
