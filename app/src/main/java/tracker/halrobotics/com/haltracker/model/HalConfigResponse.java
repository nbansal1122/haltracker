package tracker.halrobotics.com.haltracker.model;

/**
 * Created by mac on 30/07/18.
 */

public class HalConfigResponse extends BaseResponse {
    // frequency in mins;
    private double frequency;

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }
}
