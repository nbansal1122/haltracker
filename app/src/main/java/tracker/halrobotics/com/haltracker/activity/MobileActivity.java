package tracker.halrobotics.com.haltracker.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import tracker.halrobotics.com.haltracker.R;
import tracker.halrobotics.com.haltracker.model.BaseResponse;
import tracker.halrobotics.com.haltracker.util.ApiGenerator;

public class MobileActivity extends AppBaseActivity {
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile);
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit:
                if (isValid()) {
                    this.sendOtpRequest();
                }
                break;
        }
    }

    private void sendOtpRequest() {
        this.executeTask(AppConstants.TASK_CODES.SEND_OTP, ApiGenerator.sendOtp(mobile));
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.SEND_OTP:
                BaseResponse apiResponse = (BaseResponse) response;
                if(apiResponse != null && apiResponse.isStatus()){
                    Bundle b = new Bundle();
                    b.putString(AppConstants.BUNDLE_KEYS.MOBILE, mobile);
                    startNextActivity(b, OtpVerification.class);
                    finish();
                }else{
                    showToast(R.string.error_send_otp);
                }
                break;
        }
    }



    private boolean isValid() {
        mobile = getEditText(R.id.et_mobile);
        if (TextUtils.isEmpty(mobile)) {
            showToast("Mobile number can not be empty");
            return false;
        } else if (mobile.length() != 10) {
            showToast("Please enter valid mobile number of 10 digits");
            return false;
        }
        return true;
    }
}
