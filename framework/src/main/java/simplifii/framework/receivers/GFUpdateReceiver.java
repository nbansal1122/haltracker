package simplifii.framework.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nitin on 04/12/15.
 */
public class GFUpdateReceiver extends BroadcastReceiver {

    public static final String ACTION_UPDATE_REQUEST = "CART_UPDATE_REQUEST";
    public static final String ACTION_UPDATE_CHECKOUT = "CART_UPDARE_CHECKOUT";
    public static final String ACTION_UPDATE_CHECKIN = "CART_UPDATE_CHECKIN";
    public Set<GFUpdateListener> listeners = new HashSet<>();

    public static final String ACTION_REFRESH = "ACTION_REFRESH_DATA";

    public GFUpdateReceiver(GFUpdateListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        for (GFUpdateListener listener : listeners) {
            listener.onReceive(intent);
        }
    }

    public void removeListener(GFUpdateListener listener) {
        this.listeners.remove(listener);
    }

    public static interface GFUpdateListener {
        public void onReceive(Intent intent);
    }

    public static void sendBroadcast(Context context, String action) {
        Intent intent = new Intent(action);
        context.sendBroadcast(intent);
    }

    public static void sendRefreshBroadcast(Context context) {
        Intent intent = new Intent(ACTION_REFRESH);
        context.sendBroadcast(intent);
    }
}
