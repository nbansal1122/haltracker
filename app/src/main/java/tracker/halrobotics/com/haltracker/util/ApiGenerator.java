package tracker.halrobotics.com.haltracker.util;

import android.content.Context;
import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import tracker.halrobotics.com.haltracker.model.BaseResponse;
import tracker.halrobotics.com.haltracker.model.DeviceInfoRequest;
import tracker.halrobotics.com.haltracker.model.HalConfigResponse;
import tracker.halrobotics.com.haltracker.model.OtpRequest;

/**
 * Created by mac on 29/07/18.
 */

public class ApiGenerator {
    public static HttpParamObject sendOtp(String mobile){
        OtpRequest req = new OtpRequest();
        req.setPhone_number(Long.parseLong(mobile));
        req.setCountry_code("+91");
        req.setTime(new Date().getTime());
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.OTP_REQUEST);
        obj.setPostMethod();
        obj.setJSONContentType();
        obj.setJson(JsonUtil.toJson(req));
        obj.setClassType(BaseResponse.class);
        return obj;
    }

    public static HttpParamObject verifyOtp(String mobile, String code){
        OtpRequest req = new OtpRequest();
        req.setPhone_number(Long.parseLong(mobile));
        req.setCountry_code("+91");
        req.setTime(new Date().getTime());
        req.setCode(Long.parseLong(code));
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.OTP_VERIFICATION);
        obj.setPostMethod();
        obj.setJSONContentType();
        obj.setClassType(BaseResponse.class);
        obj.setJson(JsonUtil.toJson(req));
        return obj;
    }

    public static HttpParamObject postDeviceInfo(Context context, String mobileNumber){
        DeviceInfoRequest req = new DeviceInfoRequest();
        req.setId(Util.getAndroidId(context));
        req.setPhone_number(Long.parseLong(mobileNumber));
        req.setToken("APA91bFnzuekldXDXVQTyr6i54dHSrBcIrqK8I- 2j6InVrYV4WwO6WDHLMm1XtsHRWiplhBgWdkI9Rem145eQbKrYp1INTZcMqUJtYUAybUqY3lG1rqK3nYT M_GKJCO9ZfRxZCGtQlcYHvoPX3vQLsq");
        req.setOsv(Build.MANUFACTURER);
        req.setModal(Build.MODEL);
        req.setName(Build.BRAND);

        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.DEVICE_INFO);
        obj.setPostMethod();
        obj.setClassType(BaseResponse.class);
        obj.setJSONContentType();
        obj.setJson(JsonUtil.toJson(req));
        return obj;
    }

    public static HttpParamObject getConfigData(String mobileNumber){
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_CONFIG+mobileNumber);
        obj.setClassType(HalConfigResponse.class);
        return obj;
    }

    public static HttpParamObject postLocationData(JSONArray locationArray) throws JSONException {
        String phone_number = Preferences.getData(Preferences.KEY_MOBILE, "");
        JSONObject json = new JSONObject();
        json.put("phone_number", Long.parseLong(phone_number));
        json.put("time", System.currentTimeMillis());
        json.put("location", locationArray);
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.POST_DATA);
        obj.setPostMethod();
        obj.setClassType(HalConfigResponse.class);
        obj.setJSONContentType();
        obj.setJson(json.toString());
        return obj;
    }


}
