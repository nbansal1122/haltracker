package simplifii.framework.asyncmanager;

import org.json.JSONException;

import java.io.IOException;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import simplifii.framework.exceptionhandler.RestException;

/**
 * Created by rajnikant on 7/9/17.
 */

public class FormDataService extends OKHttpService {
private String line="\r\n";
    @Override
    public Object getData(Object... params) throws IOException, RestException, JSONException {
        if (params != null && params.length > 0) {
            HttpParamObject param = (HttpParamObject) params[0];
            OkHttpClient client = new OkHttpClient();
            StringBuilder stringBuilder=new StringBuilder();
            for (Map.Entry<String, String> pair : param.getPostParams().entrySet()) {
                stringBuilder
                        .append(line+"Content-Disposition: form-data; name=\""+pair.getKey()+"\""+line+line+pair.getValue()+line+"------WebKitFormBoundary7MA4YWxkTrZu0gW");
            }

            MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW" +
                    stringBuilder.toString()+"--");
            Request.Builder builder = new Request.Builder()
                    .url(param.getUrl())
                    .post(body);
            // Add Headers
            for (Map.Entry<String, String> pair : param.getHeaders().entrySet()) {
                builder.addHeader(pair.getKey(), pair.getValue());
            }

            Response response = client.newCall(builder.build()).execute();
            if (response.isSuccessful()) {
                return parseJson(response.body().string(), param);
            } else {
                throw new RestException(response.code(), getResponseReason(response.body().string()));
            }
        } else {
            throw new IllegalArgumentException();
        }
    }
}
