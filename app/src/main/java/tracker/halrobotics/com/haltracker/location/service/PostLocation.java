package tracker.halrobotics.com.haltracker.location.service;

import android.content.Context;
import android.location.Location;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simplifii.framework.utility.Preferences;
import tracker.halrobotics.com.haltracker.util.DistanceUtil;
import tracker.halrobotics.com.haltracker.util.TMUtil;

public class PostLocation {
    private static final String TAG = "PostLocation";
    private static final long SEC_90 = 90000;

    public PostLocation() {
    }

    public void markLocation(final Context context, final Location location, final Map map) {
        if (location == null) {
            return;
        }
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        } else {
            hashMap = new HashMap(map);
        }


        hashMap.put("latitude", location.getLatitude());
        hashMap.put("altitude", location.getAltitude());
        hashMap.put("longitude", location.getLongitude());
        hashMap.put("accuracy", location.getAccuracy() + "");
        hashMap.put("provider", location.getProvider() + "");
        hashMap.put("time", System.currentTimeMillis());
        hashMap.put("timeFromLocationApi", location.getTime());
        float speed = 0.0f;
        if(location.hasSpeed()){
            speed = location.getSpeed();
        }if (speed > 0.0f) {
            speed = speed * 3.6f;
        }
        hashMap.put("speed", speed + "km/h");
        hashMap.put("bearing", location.getBearing() + "");
        hashMap.put("deviceTime", System.currentTimeMillis());
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {
            hashMap.put("simOperator", manager.getSimOperator() + "");
            hashMap.put("networkOperator", manager.getNetworkOperator() + "");
        }
        String calSpeed = getSpeedViaCalculation(hashMap);
        hashMap.put("calculatedSpeed", calSpeed);

        TMUtil.setBatteryInfo(context, hashMap);
        TMUtil.setSignalStrength(context, hashMap);
//        hashMap.put("batteryPercentage", TMUtil.getBatteryPercentage(context));

        Preferences.saveLocation(new JSONObject(hashMap));
        Log.d(TAG, hashMap + "");
//        Toast.makeText(context, "Speed: "+speed + "\nCalculated Speed: "+calSpeed, Toast.LENGTH_SHORT).show();
    }

    public static String getSpeedViaCalculation(HashMap hashMap) {
        try {
            JSONObject lastLocation = Preferences.getLastSavedLocation();
            if (lastLocation != null) {

                long lastTime = lastLocation.getLong("time");
                long currentTime = System.currentTimeMillis();

                // current time should not be less than last time + 1.5 minutes
//                long maxTime = lastTime + SEC_90;
                if (currentTime > lastTime) {
                    double lastLat = lastLocation.getDouble("latitude");
                    double lastLng = lastLocation.getDouble("longitude");
                    double currentLat = (double) hashMap.get("latitude");
                    double currentLng = (double) hashMap.get("longitude");
                    // distance in Km
//                    double distance = DistanceUtil.CalculateDistanceBetweenLocations(lastLat, currentLat, lastLng, currentLng);
                    double distance = DistanceUtil.distance(lastLat, lastLng, currentLat, currentLng);
                    hashMap.put("calculatedDistanceInKm", distance);
                    double timeDifferenceInHour = ((currentTime - lastTime)*1.0) / (1000 * 60 * 60);
                    Log.d(TAG, "CurrentTime" + currentTime);
                    Log.d(TAG, "LastTime" + lastTime);
                    Log.d(TAG, "Diff: Current-last" + (currentTime - lastTime));
                    Log.d(TAG, "TimeDifference" + timeDifferenceInHour);

                    return (distance / (timeDifferenceInHour)) + "km/h";
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "0.0km/h";
    }

}
