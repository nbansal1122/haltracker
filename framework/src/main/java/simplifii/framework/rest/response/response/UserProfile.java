package simplifii.framework.rest.response.response;

import android.text.TextUtils;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

public class UserProfile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private Long mobile;
    @SerializedName("role")
    @Expose
    private String role;
    private String fk_org;
    private String device_token;
    private String gender;
    private String is_verified;
    private String login_datetime;
    private int rm;
    private String center;
    //    private String unique_code;
    private String last_attendance_recorded_at;

    public String getLast_attendance_recorded_at() {
        return last_attendance_recorded_at;
    }

    public void setLast_attendance_recorded_at(String last_attendance_recorded_at) {
        this.last_attendance_recorded_at = last_attendance_recorded_at;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;
    private String hypertrack_secret_key;
    private String hypertrack_public_key;
    //private boolean isTrackingEnabled;
    //private boolean isHyperTrackingEnabled;
    private int isHyperTrackEnabledForThisUser;
private String unique_code;

    public String getHypertrack_secret_key() {
        return hypertrack_secret_key;
    }

    public void setHypertrack_secret_key(String hypertrack_secret_key) {
        this.hypertrack_secret_key = hypertrack_secret_key;
    }

    public String getHypertrack_public_key() {
        return hypertrack_public_key;
    }

    public void setHypertrack_public_key(String hypertrack_public_key) {
        this.hypertrack_public_key = hypertrack_public_key;
    }

    private JSONObject jsonObject;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public UserProfile() {
    }

   /* public boolean getIsTrackingEnabled() {
        return isTrackingEnabled;
    }

    public void setIsTrackingEnabled(boolean isTrackingEnabled) {
        this.isTrackingEnabled = isTrackingEnabled;
    }*/

    /*public boolean getIsHyperTrackingEnabled() {
        return isHyperTrackingEnabled;
    }

    public void setIsHyperTrackingEnabled(boolean isHyperTrackingEnabled) {
        this.isHyperTrackingEnabled = isHyperTrackingEnabled;
    }*/

    public int getIsHyperTrackEnabledForThisUser() {
        return isHyperTrackEnabledForThisUser;
    }

    public void setIsHyperTrackEnabledForThisUser(int isHyperTrackEnabledForThisUser) {
        this.isHyperTrackEnabledForThisUser = isHyperTrackEnabledForThisUser;
    }

    public UserProfile(UserProfile userProfile) {
        if (userProfile == null)
            return;
        this.id = userProfile.getId();
        this.name = userProfile.getName();
        this.profileImage = userProfile.getProfileImage();
        this.email = userProfile.getEmail();
        this.mobile = userProfile.getMobile();
        this.role = userProfile.getRole();

        this.fk_org = userProfile.getFk_org();
        this.device_token = userProfile.getDevice_token();
        this.gender = userProfile.getGender();
        this.is_verified = userProfile.getIs_verified();
        this.login_datetime = userProfile.getLogin_datetime();
        this.rm = userProfile.getRm();
        this.center = userProfile.getCenter();
        //       this.unique_code = userProfile.getUnique_code();
        //       this.isHyperTrackingEnabled = userProfile.getIsHyperTrackingEnabled();
        //       this.isTrackingEnabled = userProfile.getIsTrackingEnabled();
        this.isHyperTrackEnabledForThisUser = userProfile.getIsHyperTrackEnabledForThisUser();
        this.username = userProfile.getUsername();
        this.last_attendance_recorded_at = userProfile.getLast_attendance_recorded_at();
    this.unique_code=userProfile.getUnique_code();

    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getRm() {
        return rm;
    }

    public void setRm(int rm) {
        this.rm = rm;
    }

    public String getFk_org() {
        return fk_org;
    }

    public void setFk_org(String fk_org) {
        this.fk_org = fk_org;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getLogin_datetime() {
        return login_datetime;
    }

    public void setLogin_datetime(String login_datetime) {
        this.login_datetime = login_datetime;
    }


    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

//    public String getUnique_code() {
//        return unique_code;
//    }

//    public void setUnique_code(String unique_code) {
//        this.unique_code = unique_code;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }


    public void getNameChar(TextView tvNameChar) {
        if (name != null) {
            StringBuilder initials = new StringBuilder();
            for (String s2 : name.split(" ")) {
                initials.append(s2.charAt(0));
            }
            tvNameChar.setText(initials.toString().toUpperCase());
        }
    }


    public static UserProfile instance;

    public static UserProfile getSavedInstance() {
//        if (instance == null) {
//            String json = Preferences.getData(Preferences.USER_PROFILE, null);
//            if (!TextUtils.isEmpty(json)) {
//                instance = (UserProfile) JsonUtil.parseJson(json, UserProfile.class);
//                if (instance != null) {
//                    try {
//                        instance.setJsonObject(new JSONObject(json));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
        return instance;
    }

}