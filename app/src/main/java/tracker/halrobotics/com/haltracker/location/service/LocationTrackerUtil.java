package tracker.halrobotics.com.haltracker.location.service;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

import simplifii.framework.utility.Preferences;

import static android.content.Context.ALARM_SERVICE;


public class LocationTrackerUtil {
    //    static int SCHEDULAR_TIME_MINIMUM = 60 * 1000;
//    static int SCHEDULAR_OVERRIDE_TIME = 80 * 1000;
    public static int TIME_MULTIPLIYEER = 1000 * 30 * 1;

    public static String EncodeString(String string) {
        return string.replace(".", ",");
    }

    public static String DecodeString(String string) {
        return string.replace(",", ".");
    }

    // schedule the start of the service every 10 - 30 seconds
    public static void scheduleJob(Context context) {
        if(!Preferences.isUserLoggerIn()){
            return;
        }
        long interval = 1000*60*15;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ComponentName serviceComponent = new ComponentName(context, SyncJobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(8767, serviceComponent);
            builder.setMinimumLatency((long) interval); // wait at least
            PersistableBundle bundle = new PersistableBundle();
            bundle.putDouble("interval", interval);
            builder.setExtras(bundle);
            builder.setOverrideDeadline((long) (interval * 1.1)); // maximum delay
            builder.setPersisted(true);
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
            JobScheduler jobScheduler = null;
            jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.cancelAll();
            //      jobScheduler.cancel(8767);
            jobScheduler.schedule(builder.build());
            Log.e(context.getPackageName(), "Job Scheduled");
        } else {
            Intent i = new Intent(context, LocationMonitoringService.class);
            PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
            AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            am.cancel(pi); // cancel any existing alarms
            am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + interval,
                    interval, pi);
            Log.e(context.getPackageName(), "Job Scheduled Alarm Manager");
        }
    }

    public static void startTracker(Context context) {
        if(Preferences.isUserLoggerIn()){
            Intent intent1 = new Intent(context, LocationMonitoringService.class);
            if (!isMyServiceRunning(context, LocationMonitoringService.class)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(intent1);
                } else {
                    context.startService(intent1);
                }
            }
        }
    }


    public static void stopTracker(Context context) {
        Intent intent1 = new Intent(context, LocationMonitoringService.class);
        context.stopService(intent1);
        Log.e(context.getPackageName(), "stopTracker");
    }

    public static void stopSchedular(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            JobScheduler jobScheduler = null;
            jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.cancelAll();
            Log.e(context.getPackageName(), " stopSchedular Job Stop");
        } else {
            Intent i = new Intent(context, LocationMonitoringService.class);
            PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
            AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            am.cancel(pi); // cancel any existing alarms
            Log.e(context.getPackageName(), " stopSchedular Job Stop Alarm Manager");
        }
    }


    private static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSimplifiiTrackingEnabled() {
        return true;
    }

    public static boolean checkInternet(Context context) {
        if (isNetworkAvailable(context)) {
            return isInternetAvailable();
        }
        return false;
    }

    private static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private static boolean isInternetAvailable() {
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            return false;// Log error
        }
    }


}

