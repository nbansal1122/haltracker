package tracker.halrobotics.com.haltracker.location.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import tracker.halrobotics.com.haltracker.R;


public class CustomNotification {
    private int currentNotificationID;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private String notificationTitle;
    private String notificationText;
    Context context;
    Intent notificationIntent;

    public CustomNotification(Context context, Intent notificationIntent, int currentNotificationID, String notificationTitle, String notificationText) {
        this.context = context;
        this.notificationIntent = notificationIntent;
        this.notificationText = notificationText;
        this.notificationTitle = notificationTitle;
        this.currentNotificationID = currentNotificationID;
    }

    public void sendNotification() {
        setDataForSimpleNotification();
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        clearAllNotifications();
        notificationManager.notify(currentNotificationID, notification);
    }

    private void clearAllNotifications() {
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }


    private void setDataForSimpleNotification() {
        notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                //   .setLargeIcon(R.mipmap.sf_app_icon)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText);
    }

}
